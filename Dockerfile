FROM registry.gitlab.com/sisicmbio/infra/alpine-3.9_groovy:master  AS production
MAINTAINER Manuel Ortiz Bey

USER root
# Set customizable env vars defaults.
# Set Grails version (max version for this Docker image is: 2.5.6).
ENV GRAILS_VERSION 2.5.6

# Install Grails
WORKDIR /usr/lib/jvm
RUN wget https://github.com/grails/grails-core/releases/download/v$GRAILS_VERSION/grails-$GRAILS_VERSION.zip && \
    unzip grails-$GRAILS_VERSION.zip && \
    rm -rf grails-$GRAILS_VERSION.zip && \
    ln -s grails-$GRAILS_VERSION grails

# Setup Grails path.
ENV GRAILS_HOME /usr/lib/jvm/grails
ENV PATH $GRAILS_HOME/bin:$PATH

## Create App Directory
#RUN mkdir /app
## Set Workdir
WORKDIR /

EXPOSE 8080

RUN grails create-app app

RUN ls -la


WORKDIR /app

# Set Default Behavior
ENTRYPOINT ["grails"]
CMD ["run-app"]

